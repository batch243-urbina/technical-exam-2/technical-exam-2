let collection = [];

// Write the queue functions below.

function print() {
  // It will show the array
  return collection;
}

function enqueue(element) {
  //In this function you are going to make an algorithm that will add an element to the array
  // Mimic the function of push method
  collection[collection.length] = element;
  return collection;
}

function dequeue() {
  // In here you are going to remove the first element in the array
  for (let i = 1; i < collection.length; i++) {
    collection[i - 1] = collection[i];
  }
  collection.length--;
  return collection;
}

function front() {
  // In here, you are going to get the first element
  const firstElement = collection[0];
  return firstElement;
}

// starting from here, di na pwede gumamit ng .length property
function size() {
  // Number of elements
  let i = 0;
  let count = 0;

  while (collection[i]) {
    count += 1;
    i++;
  }
  return count;
}

function isEmpty() {
  //it will check whether the function is empty or not

  if (collection === null) {
    return true;
  } else {
    return false;
  }
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
